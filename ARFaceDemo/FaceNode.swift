//
//  FaceNode.swift
//  ARFaceDemo
//
//  Created by Julia Vashchenko on 29/05/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

import ARKit

class FaceNode: SCNNode {
    init(device: MTLDevice, color: UIColor = .white) {
        let faceGeometry = ARSCNFaceGeometry(device: device)
        if let material = faceGeometry?.firstMaterial {
            material.diffuse.contents = UIColor.lagoon
        }
        super.init()
        self.geometry = faceGeometry
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(with faceAnchor: ARFaceAnchor) {
        guard let faceGeometry = geometry as? ARSCNFaceGeometry else {return}
        faceGeometry.update(from: faceAnchor.geometry)
    }
}


extension UIColor {
    static let lagoon = UIColor(red: 99/255, green: 202/255, blue: 208/255, alpha: 1.0)
}
