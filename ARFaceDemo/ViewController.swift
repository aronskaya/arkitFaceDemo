//
//  ViewController.swift
//  ARFaceDemo
//
//  Created by Julia Vashchenko on 29/05/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var trackingStateLabel: UILabel!
    var faceNode: FaceNode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        let configuration = ARFaceTrackingConfiguration()
        sceneView.session.run(configuration)
    }

    override func viewWillAppear(_ animated: Bool) {
        guard ARFaceTrackingConfiguration.isSupported, let device = sceneView.device else {
            print("ARFaceTrackingConfiguration is not supported on this device!")
            navigationController?.popViewController(animated: true)
            return
        }
        super.viewWillAppear(animated)
        faceNode = FaceNode(device: device)

        sceneView.session.run(ARFaceTrackingConfiguration(), options: [.resetTracking, .removeExistingAnchors])
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        sceneView.session.pause()
    }
}

extension ViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor else {
            fatalError()
        }

        faceNode?.removeFromParentNode()
        if faceNode != nil {
             node.addChildNode(faceNode!)
        }

        faceNode?.update(with: faceAnchor)
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor else {
            fatalError()
        }

        faceNode?.update(with: faceAnchor)
    }

    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        faceNode?.removeFromParentNode()
    }

    // MARK: - ARSessionObserver

    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        trackingStateLabel.text = camera.trackingState.description
    }
}

